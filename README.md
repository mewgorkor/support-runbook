# Gitter Support Runbook

Unifying processes for handling support requests for Gitter.im.

We'll merge this runbook with the main [Gitter repo](https://gitlab.com/gitlab-org/gitter/webapp/) when it gets more stable.

## Renaming/moving a room

<a id="rename-move-room"></a>

Before renaming the room, make sure the new name is not occupied. If the user already created the rooms, you can [delete them](#delete-room) if they are empty.

1. [Connect to webapp box](https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/utility-scripts.md#wrench-before-you-can-run-a-script-wrench)
1. Run the `rename-room.js` script:

    ```sh
    ./scripts/utils/rename-room.js -o old-community/old-room -n new-community/new-room
    ```

    - `old-community` and `new-community` can be the same if you are only renaming the room
    - if you run this command for a room associated with GitHub repo, you'll have to use `-f` option

### Renaming/moving a room associated with a GitHub repository

This rename consist of two steps:

1. [Associate the room with new GitHub repository](#associate-github-repo)
1. [Rename/move the room](#rename-move-room)

#### Zendesk examples

- [Move Gitter room to a new community and associate it with moved GitHub repo](https://gitter.zendesk.com/agent/tickets/13892)

## Associate Gitter room/community with a GitHub repository

<a id="associate-github-repo"></a>

This process works for both:

- associating a repo to originally non-GitHub based room/community
- changing which repo is associated to a GitHub based room/community

Replace `troupes` with `groups` if you wish to change community instead of a room.

1. [Get `externalId` based on `linkPath`](#link-path-github-repo)
1. [Connect to the production database](#prod-db)
1. Run the following update replacing `uri`, `linkPath`, and `externalId`:

    ```js
    db.troupes.update({uri: 'docker/docker-ce'}, {$set: {
            "sd.externalId": 91851756,
            "sd.linkPath" : "docker/docker-ce",
            "sd.type": "GH_REPO",
            "sd.members" : "GH_REPO_ACCESS",
            "sd.admins" : "GH_REPO_PUSH"
     }});
    ```

1. You can validate that the change was successful by running

    ```js
    db.troupes.find({"uri":"<group uri>"}).pretty();
    ```

1. And you are done.

## Associate Gitter community with a GitHub org

<a id="associate-github-org"></a>

Communities (internally `groups`) can be associated with a single GitHub organisation.

1. [Get `externalId` based on `linkPath`](#link-path-github-org)
1. [Connect to the production database](#prod-db)
1. Run the following update replacing `uri`, `linkPath`, and `externalId`ß:

    ```js
    db.groups.update({uri: 'docker'}, {$set: {"sd": {
		"externalId" : "91851756",
		"linkPath" : "docker",
		"public" : true,
		"admins" : "GH_ORG_MEMBER",
		"members" : "PUBLIC",
		"type" : "GH_ORG"
    }}});
    ```

1. You can validate that the change was successful by running

    ```js
    db.groups.find({"uri":"<group uri>"}).pretty();
    ```

1. And you are done.

### Zendesk examples

- [Associate existing Gitter community with a GitHub org](https://gitter.zendesk.com/agent/tickets/13945)

## Connecting to the production database

<a id="prod-db"></a>

1. SSH into the box `ssh mongo-replica-01.prod.gitter`
1. Open gitter database with `mongo gitter`

## Delete a room

<a id="delete-room"></a>

Room admins are [capable of removing the room](https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/rooms.md#delete-a-room) and normally there shouldn't be a need for support team to do it.

You can still delete a room for a user (e.g. because you want to avoid asking them to do so and cause another round trip in communication).

1. [Connect to webapp box](https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/utility-scripts.md#wrench-before-you-can-run-a-script-wrench)
1. Run the `delete-room.js` script:

    ```sh
    ./scripts/utils/delete-room.js -u "community/room"
    ```

## Manually create a community associated to a user

This is process that is temporarily in place because users are unable to create a community associated with their own username: https://gitlab.com/gitlab-org/gitter/webapp/-/issues/2462

Take the following snippet (where `gitterUser` is a placeholder for the user's username):

```js
db.groups.create({
    "lcUri" : "gitteruser",
    "sd" : {
        "extraAdmins" : [ ],
        "externalId" : db.users.findOne({username: "gitterUser"}).githubId,
        "linkPath" : "gitterUser",
        "public" : true,
        "admins" : "GH_USER_SAME",
        "members" : "PUBLIC",
        "type" : "GH_USER"
    },
    "uri" : "gitterUser",
    "name" : "gitterUser",
    "homeUri" : "gitterUser/home",
    "lcHomeUri" : "gitteruser/home"
});
```

1. Replace `lcUri` (lower case URI) and part of `lcHomeUri` with the username in lower case letters
2. Replace all other occurrences of `gitterUser` with the username
3. [Connect to the production database](#prod-db)
4. Paste the edited snippet and run it

## Invalidate user's access token

If you see that user access token was mentioned publicly (i.e. it was compromised), the easiest way to invalidate it is to sign in to gitter using then token and signing out immediately.

Use: `https://gitter.im/?access_token=<compromised access token>`

## Resolve E11000 duplicate GitHub ID error on user sign in

This process is concerned with support tickets containing error message:

```
(E11000 duplicate key error collection: gitter.users index: githubId_1 dup key: { : <GitHub id> })

(E11000 duplicate key error collection: gitter.users index: username_1 dup key: { : "someone_gitlab" })
```

This error is caused by user having two GitHub users e.g. `alice` and `bob`. And deleting or renaming one of them (delete `alice`) and renaming the other account to use the original username (rename `bob` -> `alice`).

- https://gitlab.com/gitlab-org/gitter/webapp/-/issues/1127
- https://gitlab.com/gitlab-org/gitter/webapp/-/issues/1852

### Process

1. Request more information from the user by sending them:
    ```
    This usually happens after you have done some tricky renaming, see https://gitlab.com/gitlab-org/gitter/webapp/issues/1127

    Can you explain the whole situation on what users you renamed to what and deleted, etc?

    Also please reply with the primary email associated with those old accounts to verify you did control them. Just cc them into this ticket and reply.
    ```
1. Wait till they respond
1. Find the user with the `githubId`: `db.users.find({githubId: <id>}).pretty()`
1. Check how many messages the user have sent: `db.chatmessages.find({fromUserId: ObjectId("<id>")}).count()`
1. If the user sent more than a couple of messages, we should require further [verification from the email associated with the Gitter user we're about to delete](#authenticate-zendesk-request)
1. [Connect to webapp box](https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/utility-scripts.md#wrench-before-you-can-run-a-script-wrench)
1. Delete the user `./delete-user.js -u <username>`
1. Now change `username` and remove the `githubId`

    ```js
    db.users.update({username: "<username>"}, {$set: {
            "username":"<username>~removed"
     },$unset: {
         "githubId": ""
     }});
    ```

*Potentially remove other user with the email from Zendesk request, this might not be necessary ⚠️*

## Authenticate Zendesk request

When you receive Zendesk ticket that request manipulating user data, you should validate that the Zendesk requester is identical to the Gitter user.

1. [Connect to webapp box](https://gitlab.com/gitlab-org/gitter/webapp/-/blob/develop/docs/utility-scripts.md#wrench-before-you-can-run-a-script-wrench)
1. Run `./find-users-by-email.js -e <email from the ticket>` and validate that the email is associated with the Gitter user.

## Finding `externalId` based on `linkPath`

These two attributes are used in Gitter database to link communities and rooms to GitLab and GitHub.

 - `linkPath` is the URI part of the GitHub org or repo (e.g. `https://github.com/gitterHQ/gitter` -> `gitterHQ/gitter`)
 - `externalId` is the GitHub ID (e.g. they will look like `5990364`)

You can use the GitHub API to get these values and use them in your MongoDB query.

### GitHub org

<a id="link-path-github-org"></a>

```bash
curl -s https://api.github.com/orgs/gitterHQ | jq ".login,.id"
"gitterHQ" # linkedPath
5990364 # externalId
```

### GitHub repo

<a id="link-path-github-repo"></a>

```bash
curl -s https://api.github.com/repos/gitterHQ/gitter | jq ".full_name,.id"
"gitterHQ/gitter" # linkedPath
14863998 # externalId
```
